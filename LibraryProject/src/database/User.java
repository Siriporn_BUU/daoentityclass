/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author Admin
 */
public class User {
    int userID ;
    String loginName ;
    String password ;
    String name ;
    String surname ;
    int typeID ;

    public User() {
        this.userID = -1 ;
    }

    public User(int userID, String loginName, String password, String name, String surname, int typeID) {
        this.userID = userID;
        this.loginName = loginName;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.typeID = typeID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    @Override
    public String toString() {
        return "User{" + "userID=" + userID + ", loginName=" + loginName + ", password=" + password + ", name=" + name + ", surname=" + surname + ", typeID=" + typeID + '}';
    }
    
}
