/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import libraryproject.LibraryProject;

/**
 *
 * @author Admin
 */
public class UserDao {
    public static boolean insert(User user) {
        Connection conn = database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO user (\n" +
"                    loginName ,\n" +
"                    password,\n" +
"                    name,\n" +
"                    surname,\n" +
"                    typeID\n" +
"                 )\n" +
"                 VALUES (\n" +
"                     '%s',\n" +
"                     '%s',\n" +
"                     '%s',\n" +
"                     '%s',\n" +
"                     %d\n" +
"                 );";
            stm.execute(String.format(sql, user.getLoginName() , user.getPassword() , 
                    user.getName() , user.getSurname() , user.getTypeID()));
            database.close();
            return true ;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.close();
        return true ;
    }
    public static boolean update(User user) {
        String sql = "UPDATE user SET \n" +
                    " loginName = '%s',\n" +
                    " password = '%s',\n" +
                    " name = '%s',\n" +
                    " surname = '%s',\n" +
                    " typeID = %d\n" +
                    " WHERE userID = %d;";
        Connection conn = database.connect();
        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, 
                    user.getLoginName(),
                    user.getPassword(),
                    user.getName(),
                    user.getSurname(),
                    user.getTypeID(),
                    user.getUserID()
            ));
            database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.close();
        return false ;
    }
    public static boolean delete(User user) {
        String sql = "DELETE FROM user WHERE userID = %d;";
        Connection conn = database.connect();
        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, user.getUserID()));
            database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.close();
        return true ;
    }
    public static ArrayList<User> getUsers() {
        ArrayList<User> list = new ArrayList();
        Connection conn = database.connect() ;
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT userID,\n" +
                    "       loginName,\n" +
                    "       password,\n" +
                    "       name,\n" +
                    "       surname,\n" +
                    "       typeID\n" +
                    "  FROM user;" ;
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("userID") + " " + rs.getString("loginName"));
                User user = toObject(rs);
                list.add(user);
            }
            database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(LibraryProject.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.close();
        return null ;
    }

    public static User toObject(ResultSet rs) throws SQLException {
        User user;
        user = new User() ;
        user.setUserID(rs.getInt("userID"));
        user.setLoginName(rs.getString("loginName"));
        user.setPassword(rs.getString("password"));
        user.setName(rs.getString("name"));
        user.setSurname(rs.getString("surname"));
        user.setTypeID(rs.getInt("typeID"));
        return user ;
    }
    public static User getUser(int userID) {
        String sql = "SELECT * FROM user  WHERE userID = %d;";
        Connection conn = database.connect();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, userID));
            if(rs.next()) {
                User user = toObject(rs) ;
                database.close();
                return user ;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        database.close();
        return null ;
    }
    
}
